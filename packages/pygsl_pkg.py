#!/usr/bin/env python
from contractor import *

from python import python, python_internal
from numpy_pkg import numpy
from gsl import gsl

pygsl_url = "http://downloads.sourceforge.net/project/pygsl/pygsl/pygsl-0.9.5/pygsl-0.9.5.tar.gz"

# following function is a modified version of 
# contractor.need_internal_version_python_module
# that uses module.version instead of module.__version__
def need_internal_version_python_module_mod(module,min_version_list):
    cmd = 'python -c "import %s; print %s.version"' % (module,module)
    if global_runtime_options.debug_config:
        print "debug_config: running command",cmd
    (status,output) = commands.getstatusoutput(cmd)
    if global_runtime_options.debug_config:
        print "debug_config: command completed with status",status,\
            "and output:"
        print output
    if status:
        found_good_version = False
    else:
        found_good_version = version_acceptable(output,min_version_list)
    if found_good_version:
        retval = 0
    else:
        retval = 1
    return retval

def need_pygsl():
    if python_internal.get():
        retval = 1
    else:
        retval = need_internal_version_python_module_mod("pygsl", [0, 9, 1])
    return retval

pygsl_internal = Option(local_root, "pygsl_internal", need_pygsl, int,
                      "Build/use internal version of pygsl")

if pygsl_internal.get():
    pygsl = Package(local_root, 'pygsl',
                    (Unpack(url=pygsl_url), Py_install()),
                    [python, numpy, gsl])

else:
    pygsl = External_package('pygsl')
