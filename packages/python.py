#!/usr/bin/env python
from contractor import *
from commands import getstatusoutput
 
def python_get_include_dir(vars):
    cmd = 'python -c \'import distutils.sysconfig,sys;sys.stdout.write("%s\\n" % distutils.sysconfig.get_python_inc())\''
    (status,output) = getstatusoutput(cmd)
    if status != 0:
        raise RuntimeError, \
              "python package python_get_include_dir failed with output '%s'"  % \
              output
    return output

def need_python():
    # ditch if we don't even have a recent python
    if need_internal_version_executable("python -V",[2,6]):
        # there is no acceptable python installed.  Better build one.
        return 1
    # now see if we have a development environment
    try:
        inc_dir = python_get_include_dir({})
    except:
        # any problem means no python
        return 1
    # check version in header files
    return need_internal_version_library(inc_dir + '/patchlevel.h',
                                         'if (PY_VERSION_HEX < 0x02040000) return 1;')
    
python_internal = Option(local_root,"python_internal",need_python,int,
                         "Install internal version of python")

python_url = "http://www.python.org/ftp/python/2.7.4/Python-2.7.4.tar.bz2"


def python_get_version(vars):
        cmd = 'python -c \'import distutils.sysconfig,sys;sys.stdout.write("%s\\n" % distutils.sysconfig.get_python_version())\''
        (status,output) = getstatusoutput(cmd)
        if status != 0:
            raise RuntimeError, \
                "python package python_get_version failed with output '%s'"  % \
                output
        return output

def python_get_libdir(vars):
        cmd = 'python -c \'import distutils.sysconfig,sys;sys.stdout.write("%s\\n" % distutils.sysconfig.get_python_lib())\''
        (status,output) = getstatusoutput(cmd)
        if status != 0:
                raise RuntimeError, \
                      "python package python_get_lib failed with output '%s'"  % \
                      output
        return output


def get_site_packages(vars=None):
    cmd = 'python -c \'import distutils.sysconfig,sys;sys.stdout.write("%s\\n" % distutils.sysconfig.get_python_lib(plat_specific=True,prefix="%(root.install_dir)s"))\''
    (status,output) = getstatusoutput(cmd)
    if status != 0:
        raise RuntimeError, \
            "python package python_get_version failed with output '%s'"  % \
            output
    if vars:
        output = output % vars
    return output

if python_internal.get():
    python = Package(local_root,'python',
                     [Unpack(pattern="Python-*",url=python_url),
                      Configure('--enable-shared'),
                      Make(),
                      Install(),
                      # it doesn't seem to work to have 3 Add_package_vars stages.  Only the first one happens.
#                      Add_package_vars('include_dir', python_get_include_dir),
#                      Add_package_vars('version', python_get_version),
#                      Add_package_vars('libdir', python_get_libdir)])
                      Add_package_vars(['include_dir','version','libdir','sitepackages'],
                                       [python_get_include_dir,python_get_version,
                                       python_get_libdir,get_site_packages])])
else:
    python = Package(local_root,'python',
                     [Modify_environment('PYTHONPATH',get_site_packages),
                      # it doesn't seem to work to have 3 Add_package_vars stages.  Only the first one happens.
#                      Add_package_vars('include_dir', python_get_include_dir),
#                      Add_package_vars('version', python_get_version),
#                      Add_package_vars('libdir', python_get_libdir)])
                      Add_package_vars(['include_dir','version','libdir','sitepackages'],
                                       [python_get_include_dir,python_get_version,
                                       python_get_libdir,get_site_packages])])
