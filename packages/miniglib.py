#!/usr/bin/env python
from contractor import *
import os.path
import commands

from gxx import gxx

if os.path.exists("/usr/lib64/glib-2.0/include"):
    include_flags = "-I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include"
else:
    include_flags = "-I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include"

miniglib_url = "https://compacc.fnal.gov/projects/attachments/download/8/miniglib-2.20.3.tar.gz"

def need_miniglib():
        return need_internal_version_library("glib.h",
            include_flags=include_flags)

miniglib_internal = Option(local_root,"miniglib_internal",need_miniglib,int,
                       "Install internal version of miniglib")


def get_glib_inc(vars):
    # set vars for glib-2.0 includes and libs
    (error,output) = commands.getstatusoutput("pkg-config --cflags glib-2.0")
    if error:
        # pkg-config didn't work, you probably don't have glib-2.0 in
        # which case you should go with internal miniglib.
        glib_cflags = None
    else:
        flags = output.split()
        # flags will be like -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include
        # extract the first part for the include directory
        return flags[0][2:]

def get_glibconfig_inc(vars):
    # set vars for glib-2.0 includes and libs
    (error,output) = commands.getstatusoutput("pkg-config --cflags glib-2.0")
    if error:
        # pkg-config didn't work, you probably don't have glib-2.0 in
        # which case you should go with internal miniglib.
        return None
    else:
        flags = output.split()
        # flags will be like -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include
        # extract the second part for the config include directory
        return flags[1][2:]

def get_glib_libdir(vars):
    # set vars for glib-2.0 includes and libs
    (error,output) = commands.getstatusoutput("pkg-config --libs glib-2.0")
    if error:
        # pkg-config didn't work, you probably don't have glib-2.0 in
        # which case you should go with internal miniglib.
        return None
    else:
        flags = output.split()
        # flags will be like -L/lib64 -lglib-2.0 on redhat, or
        # -lglib-2.0 on ubuntu.  if the first part starts with -L,
        # use the remaining part as the libdir
        if flags[0][0:2] == "-L":
            return flags[0][2:]
        else:
            # I'll return some default library area to satisfy the linker
            if os.path.exists("/lib64"):
                return "/lib64"
            else:
                return "/lib"

def get_glib_internal_inc(vars):
    return '%(root.install_dir)s/include/glib-2.0' % vars

def get_glib_internal_config_inc(vars):
    return '%(root.install_dir)s/lib/glib-2.0/include' % vars

def get_glib_internal_libdir(vars):
    return '%(root.install_dir)s/lib' % vars

if miniglib_internal.get():
    miniglib = Package(local_root,'miniglib',
                   [Unpack(url=miniglib_url),Configure(),Make(),Install(),
                    Add_package_vars(['inc', 'config_inc', 'libdir'],
                                     [get_glib_internal_inc,
                                      get_glib_internal_config_inc,
                                      get_glib_internal_libdir])])
else:
    miniglib = Package(local_root, 'miniglib',
                       [Add_package_vars(['inc', 'config_inc','libdir'],
                                         [get_glib_inc, get_glibconfig_inc, get_glib_libdir])],
                       [gxx])


