#!/usr/bin/env python
from contractor import *

from python import python, python_internal

def need_nose():
    if python_internal.get():
        retval = 1
    else:
        retval = need_internal_version_executable("nosetests --version",[0,11,0],
        '[0-9.][0-9.]+')
    return retval

nose_url = "http://python-nose.googlecode.com/files/nose-0.11.3.tar.gz"

nose_internal = Option(local_root,"nose_internal",need_nose,int,
                      "Build/use internal version of nose")

if nose_internal.get():
    nose = Package(local_root,'nose',
                   [Unpack(pattern='tables*',url=nose_url),
                   Build_command('check_directory', 
                    'test -d %(python.sitepackages)s || mkdir -p %(python.sitepackages)s'),
                   Py_install(extra_args=' --install-lib="%(python.sitepackages)s"')],
                   [python])
else:
    nose = External_package('nose')
