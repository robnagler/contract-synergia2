#!/usr/bin/env python
from contractor import *

from python import python, python_internal

numpy_url = "http://downloads.sourceforge.net/project/numpy/NumPy/1.5.1/numpy-1.5.1.tar.gz"

def need_numpy():
    if python_internal.get():
        retval = 1
    else:
        retval = need_internal_version_python_module("numpy",[1,2])
    return retval
    
numpy_internal = Option(local_root,"numpy_internal",need_numpy,int,
                      "Build/use internal version of numpy")

if numpy_internal.get():
    numpy = Package(local_root,'numpy',
                       (Unpack(url=numpy_url),Py_install()),[python])
else:
    numpy = External_package('numpy')
