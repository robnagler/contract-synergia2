#!/usr/bin/env python
from contractor import *
from zlib import zlib

def need_libpng():
    return need_internal_version_library("libpng.h")
    
libpng_internal = Option(local_root,"libpng_internal",need_libpng,int,
                       "Install internal version of libpng")

libpng_url = "http://prdownloads.sourceforge.net/libpng/libpng-1.2.39.tar.gz?download"

if libpng_internal.get():
    libpng = Package(local_root,'libpng',
                   [Unpack(url=libpng_url),Configure(),Make(),
                    Install()],[zlib])
else:
    libpng = External_package('libpng')
    
