#!/usr/bin/env python
from contractor import *

from chef_libs import chef_libs
from mpi4py_pkg import mpi4py
from python import python
from matplotlib_pkg import matplotlib
from pytables_pkg import pytables
from fftw import fftw
from fftw3 import fftw3
from boost import boost
from cmake import cmake
from gsl import gsl
from numpy_pkg import numpy
from pygsl_pkg import pygsl
from nose import nose
from pyparsing_pkg import pyparsing
from nlopt import nlopt
from eigen3 import eigen3

syn2_branch = Option(local_root, "synergia2/branch",
                     "master", str,
                     "Branch to use when checkout out synergia2 from repository")

syn2_checkout = Option(local_root,"synergia2/checkout",1,int,
                  "Check synergia2 out of repository instead of using tarball")
if syn2_checkout.get():
    syn2_git = 'http://cdcvs.fnal.gov/projects/synergia2'
    syn2_branch = syn2_branch.get()
    get_synergia2 = Git_clone(syn2_git,syn2_branch)
else:
    get_synergia2 = Unpack()

configure_args = ""
configure_args += " -DBoost_INCLUDE_DIR=%(boost.include_dir)s"
configure_args += " -DPYTHON_INCLUDE_PATH:PATH=%(python.include_dir)s"
configure_args += " -DPYTHON_LIBRARY:FILEPATH=-lpython%(python.version)s"
configure_args += " -DFFTW3_LIBRARY_DIRS:PATH=%(fftw3.lib_dir)s"
configure_args += " -DEIGEN3_INCLUDE_DIR:PATH=%(eigen3.include_dir)s"
configure_args += " -DCHEF_PREFIX:PATH=%(chef-libs.root.install_dir)s"
configure_args += " -DCMAKE_BUILD_TYPE=Release"

synergia2 = Package(local_root,'synergia2',
                    [get_synergia2,
                    Cmake(extra_args=configure_args),
                    Make(),Install()],
                    [chef_libs,mpi4py,python,
                     pytables,boost,cmake,gsl,eigen3,numpy,fftw3,pygsl,nose,pyparsing],
                    precious=1)
