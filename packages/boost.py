#!/usr/bin/env python
from contractor import *

from cmake import cmake
from python import python
from gxx import gxx
from zlib import zlib, zlib_internal
from glob import glob

from commands import getstatusoutput
import re
    
def need_boost():
    return need_internal_version_library("boost/version.hpp",
        "if (BOOST_VERSION < 103400) return 1;")
        
boost_internal = Option(local_root,"boost_internal",need_boost,int,
                       "Install internal version of boost")

boost_cmake = Option(local_root,"boost/cmake",0,int,
                       "Build boost with cmake instead of bjam")

def boost_get_version(vars, include_dir=None):
    if not include_dir:
        include_dir = "build/boost"
    # try to open boost header file containing version number
    try:
        bhf = open(include_dir + "/boost/version.hpp")
    except IOError:
        # don't fail if boost_internal is 0 because we the include
        # directory might not be set yet so I can't find the version number
        if not boost_internal.get():
            return '0.0.0'
        else:
            raise IOError, "Failed to read %s to get version number"%(include_dir+'/build/boost')

    bvlines  = bhf.readlines()
    # compile regular expression to look for the version number
    brex = "^#define\s+BOOST_LIB_VERSION\s+\"(.*)\"\s+$"
    brexc = re.compile(brex)
    # Look for the definition of version number
    for bvl in bvlines:
        bvmatch = brexc.search(bvl)
        if bvmatch:
            return bvmatch.group(1)
    
    # Couldn't find the version
    return '0.0.0'
    raise RuntimeError, \
        "could not locate boost version number"

def boost_get_include_dir(vars):
    # this might get more dynamic someday...
    return "%(root.install_dir)s/include" % vars

def boost_get_include_dir_cmake(vars):
    # this might get more dynamic someday...
    return "%(root.install_dir)s/include" % vars

def boost_get_lib_dir(vars):
    # this might get more dynamic someday...
    return "%(root.install_dir)s/lib" % vars

def boost_get_lib_mt_suffix(vars, lib_dir=None):
    if not lib_dir:
        lib_dir = "%(root.install_dir)s/lib" % vars
    cmd = '/bin/ls -1 %s/libboost_python*-*mt.*' % lib_dir
    (status,output) = getstatusoutput(cmd)
    mt_suffix_pattern = '-mt.*'
    mt_suffix = '-mt'
    if status != 0:
        cmd = '/bin/ls -1 %s/libboost_python*.*' % lib_dir
        (status,output) = getstatusoutput(cmd)
        mt_suffix_pattern = '\..*'
        mt_suffix = ''
    if status != 0:
        if not boost_internal.get():
            #don't bomb out if I can't find the -mt suffix and
            # boost_internal is 0
            return ('', '')
        else:
            raise RuntimeError, \
                "python package get_include_dir failed with output '%s'"  % \
                output
    retval = output.split('\n')
    if len(retval)  < 1:
        raise RuntimeError, \
            "boost_get_lib_suffix failed to find the proper library"  
    retval = retval[0]
    retval = re.sub('.*libboost_python','',retval)
    lib_suffix = re.sub(mt_suffix_pattern,'',retval)
    return lib_suffix, mt_suffix

def boost_get_lib_suffix(vars):
    lib_suffix, mt_suffix = boost_get_lib_mt_suffix(vars)
    return lib_suffix

def boost_get_mt_suffix(vars):
    lib_suffix, mt_suffix = boost_get_lib_mt_suffix(vars)
    return mt_suffix

if zlib_internal.get():
    zlib_vars = 'NO_BZIP2=1 ZLIB_INCLUDE=%(root.install_dir)s/include ZLIB_LIBPATH=%(root.install_dir)s/lib '
else:
    zlib_vars = 'NO_BZIP2=1 '

if boost_internal.get():
    if boost_cmake.get():
        boost_url = "git://gitorious.org/~denisarnaud/boost/denisarnauds-cmake.git"
        boost_branch = "cmake-1.46.1"
        boost_cmake_args = '-DBUILD_PROJECTS="python;regex;serialization;test;filesystem;system;iostreams"'
        boost_cmake_args += ' -DINSTALL_VERSIONED=OFF'
        boost_cmake_args += ' -DENABLE_STATIC=OFF -DENABLE_DEBUG=OFF'
        boost_cmake_args += ' -DMPI_COMPILER=ignoreme'
        if zlib_internal.get():
            boost_cmake_args += ' -DWITH_BZIP2:BOOL=OFF'

        boost = Package(local_root,'boost',
                        [Git_clone(url=boost_url, branch=boost_branch),
                        Cmake(extra_args=boost_cmake_args),
                        Make(),
                        Install(),
                        Add_package_vars(
                            ['version','include_dir','lib_dir','lib_suffix', 'mt_suffix'],
                            [boost_get_version,boost_get_include_dir_cmake,
                                boost_get_lib_dir,boost_get_lib_suffix, boost_get_mt_suffix])],
                        [cmake,python,zlib,gxx])
    else:
        boost_url = "http://sourceforge.net/projects/boost/files/boost/1.51.0/boost_1_51_0.tar.bz2"
        boost_pkg_args = '--with-libraries=python,regex,serialization,test,filesystem,system,iostreams'
        boost = Package(local_root,'boost',
                        [Unpack(url=boost_url),
                         Bootstrap(command='./bootstrap.sh --prefix=%(root.install_dir)s ' + 
                            boost_pkg_args),
                         Build_command('bjam_install',zlib_vars + './bjam install'),
                         Add_package_vars(
            ['version','include_dir','lib_dir','lib_suffix','mt_suffix'],
            [boost_get_version,boost_get_include_dir,
             boost_get_lib_dir,boost_get_lib_suffix, boost_get_mt_suffix])],
                        [python,zlib,gxx])
else:
    boost = External_package('boost')
    boost_include = Option(local_root,"boost/include","/usr/include",str,"boost include directory")
    # protect against spurious /usr/lib64 directory on 32 bit system (Ubuntu)
    if os.path.exists('/usr/lib64') and glob("/usr/lib64/libboost*.so"):
        default_lib = '/usr/lib64'
    else:
        default_lib = '/usr/lib'
    boost_lib = Option(local_root,"boost/lib",default_lib,str,"boost library directory")
    boost.new_var('include_dir',boost_include.get())
    boost.new_var('version',boost_get_version(None, boost_include.get()))
    boost.new_var('lib_dir',boost_lib.get())
    lib_suffix, mt_suffix = boost_get_lib_mt_suffix(None,
        lib_dir=boost_lib.get())
    boost.new_var('lib_suffix', lib_suffix)
    boost.new_var('mt_suffix', mt_suffix)
