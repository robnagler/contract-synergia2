#!/usr/bin/env python
from contractor import *

from python import python
from numpy_pkg import numpy
from libpng import libpng
from freetype import freetype

matplotlib_url = "http://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.0.1/matplotlib-1.0.1.tar.gz"

def need_matplotlib():
    return need_internal_version_python_module("matplotlib",[0,99])

matplotlib_internal = Option(local_root,"matplotlib_internal",need_matplotlib,int,
                      "Build/use internal version of matplotlib")

modify_setupext = 'echo "basedir[sys.platform].append(\'%(root.install_dir)s\')" >> setupext.py'
if matplotlib_internal.get():
    matplotlib = Package(local_root,'matplotlib',
                         [Unpack(url=matplotlib_url),
                          Build_command('Modify_setupext',modify_setupext),
                          Py_install()],
                         [python,numpy,libpng,freetype])

else:
    matplotlib = External_package('matplotlib')
